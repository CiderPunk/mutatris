package net.ciderpunk.tetrisrush;

public final class Constants {

	public static final String SmallFont = "fonts/fixedsys_small.fnt";
	public static final String ScoreFont = "fonts/fixedsys_num_large.fnt";
	public static final String AtlasPath = "atlas/tetrisrush.atlas";

	
	public static int gridHeight = 20;
	public static int gridWidth = 10;
	public static int TileSize = 16;
	public static float FadeTime = 0.2f;
	public static float FastFallRate = 0.01f;
	public static float MaxFallRate = 0.01f;
	public static float MutationTimerStart = 10f; 
	public static float MutationTimerGrace =1f; 
	public static int MaxMutation = 5;
}
