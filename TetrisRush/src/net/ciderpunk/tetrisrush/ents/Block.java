package net.ciderpunk.tetrisrush.ents;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

import net.ciderpunk.gamebase.ents.Entity;
import net.ciderpunk.gamebase.gfx.Frame;
import net.ciderpunk.gamebase.resources.IResourceUser;
import net.ciderpunk.gamebase.resources.ResourceManager;
import net.ciderpunk.tetrisrush.Constants;
import net.ciderpunk.tetrisrush.data.Coord;
import net.ciderpunk.tetrisrush.gui.GridManager;

public class Block implements IResourceUser {

	public static Frame blockFrame;
	
	Tetramino owner;
	Color colour;
	Coord offset = new Coord();
	
	public Block(Tetramino owner, Coord offset, Color col){
		this.owner = owner;
		this.colour = col;
		this.offset.set(offset);
	}

	public Block(){
		this(null, null, Color.WHITE );
	}
	
	@Override
	public void preLoad(ResourceManager resMan) {
		resMan.getAssetMan().load(Constants.AtlasPath, TextureAtlas.class);
	}

	@Override
	public void postLoad(ResourceManager resMan) {
		TextureAtlas atlas = resMan.getAssetMan().get(Constants.AtlasPath, TextureAtlas.class);
		blockFrame = new Frame(atlas.findRegion("block"),0,0);
	}

	public void draw(SpriteBatch batch, Coord loc, float alpha) {
		this.colour.a = alpha;
		blockFrame.draw(batch, (loc.x + offset.x) * Constants.TileSize , (loc.y + offset.y) * Constants.TileSize, this.colour);
		//this.colour.a = 1f;
	}

	public Coord getLocation(){
		return Coord.temp.set(owner.loc).add(this.offset);
	}

	public void fixBlock(GridManager ownerGrid) {
		ownerGrid.addBlock(getLocation(), this);
		this.offset.reset();
	}
}
