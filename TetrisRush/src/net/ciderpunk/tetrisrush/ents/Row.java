package net.ciderpunk.tetrisrush.ents;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import net.ciderpunk.tetrisrush.Constants;
import net.ciderpunk.tetrisrush.data.Coord;
import net.ciderpunk.tetrisrush.gui.GridManager;

public class Row {
	
	Block[] blocks;
	Grid owner;
	float fadeTime;
	
	public void think(float dT){
		//increment counts etc...
		if (fadeTime > 0f){
			fadeTime -= dT;
			if (fadeTime <= 0f){
				reset();
				this.owner.recycleRow(this);
			}
		}
		
	}
	
	public Block get(int x){
		return blocks[x];
	}

	public void set(int x, Block block){
		blocks[x] = block;
	}
	
	public Row(Grid owner){
		this.owner = owner;
		blocks = new Block[Constants.gridWidth];
		reset();
	}
	
	public void draw(SpriteBatch batch, int y){
		float alpha = 1f;
		if (fadeTime > 0f){
			alpha = fadeTime / Constants.FadeTime;
		}
		for (int x = 0; x < Constants.gridWidth; x++){
			Block block = blocks[x];
			if (block!= null){
				block.draw(batch, Coord.temp.set(x,y), alpha);
			}
		}	
	}
	
	public void reset(){
		fadeTime = 0f;
		for (int x = 0; x < Constants.gridWidth; x++){
			blocks[x] = null;
		}	
	}
	
	public boolean testFull(){
		if (fadeTime == 0f){
			for (int x = 0; x < Constants.gridWidth; x++){
				if (blocks[x] == null){
					return false;
				}
			}	
			fadeTime = Constants.FadeTime;
			return true;
		}
		return false;
	}

}
