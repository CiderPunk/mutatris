package net.ciderpunk.tetrisrush.ents;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;

import net.ciderpunk.tetrisrush.Constants;
import net.ciderpunk.tetrisrush.data.Coord;
import net.ciderpunk.tetrisrush.data.Direction;
import net.ciderpunk.tetrisrush.data.RotateMode;
import net.ciderpunk.tetrisrush.data.Shape;
import net.ciderpunk.tetrisrush.gui.GridManager;

public class Tetramino {
	
	
	public static Color[] mutantColours = new Color[]{ 
		new Color(0.66f,0.3f,0.9f,1f),
		new Color(198f/ 255f,106f/255f,106f/255f,1f),
		new Color(113f/ 255f,172f/255f,230f/255f,1f),
		new Color(116f/ 255f,221f/255f,156f/255f,1f),
		new Color(163f/ 255f,208f/255f,3f/255f,1f),
	
	};
	
	
	int id;
	Block[] blocks;
	Coord loc = new Coord();
	RotateMode rot;
	boolean rotated;
	boolean touchDownGrace;
	GridManager ownerGrid;
	int mutation;
	Coord temp;
	
	public Tetramino(GridManager owner) {
		this.ownerGrid = owner;
		blocks = new Block[4 + Constants.MaxMutation];
		this.mutation = 0;
		temp = new Coord();
	}
	
	public void init(int id, int mutation, Shape shape, int x, int y){
		this.id = id;
		this.mutation = mutation;
		
		loc.set(x,y);
		rot = shape.rot;
		rotated = false;
		touchDownGrace = false;
		
		for(int i = 0; i < shape.blockCoords.length; i++){
			blocks[i] = new Block(this, shape.blockCoords[i], shape.colour);
		}
		for (int m = 0; m < this.mutation; m++){
			mutate(m);
		}

	}
	

	
	private void mutate(int m) {
		this.rot = RotateMode.fourWay;
		boolean success = false;
		int target = 0;
		while (!success){
			success = true;
			target = MathUtils.random(2 + m);
			temp.set(this.blocks[target].offset).add(Coord.directions[MathUtils.random(3)]);
			for(int i = 0; i < 4 + m ; i++){
				if (temp.Equal(this.blocks[i].offset)){
					success = false;
					break;
				}
			}
		}
		blocks[4+m] =  new Block(this,temp, mutantColours[MathUtils.random(mutantColours.length-1)]);
	}

	public void draw(SpriteBatch batch, boolean preview){
		for(int i = 0; i < 4 + this.mutation; i++){
			blocks[i].draw(batch, (preview ?  Coord.zero : loc),1f);
		}
	}

	public void draw(SpriteBatch batch){
		this.draw(batch, false);
	}

	public void move(int i) {
		this.loc.x += i;
		if (testCollision()){
			this.loc.x -= i;
		}
	}

	public boolean rotate(boolean clockwise){
		switch (rot){
			case oneWay:
				return true;
			case twoWay:
				this.rotated = !this.rotated;
				return this.tryRotate(this.rotated);
			default:
				return this.tryRotate(clockwise);
		}
	}

	protected boolean tryRotate(boolean cw){
		doRotate(cw);
		if (testCollision()){
			doRotate(!cw);
			return false;
		}
		return true;
	}

	protected void doRotate(boolean cw) {
		for(int i = 0; i < 4 + mutation; i++){
			if (cw){
				blocks[i].offset.rotateCW();
			}
			else{
				blocks[i].offset.rotateCCW();
			}
		}	
	}
	
	
	/**
	 * checks if the tetrimino collides wuith any existing structures
	 * @return
	 */
	protected boolean testCollision(){
		for(int i = 0; i < 4 + mutation; i++){
			if (ownerGrid.isOccupied(blocks[i].getLocation())){
				return true;
			}
		}
		return false;
	}

	public void fallDown() {
		this.loc.y--;
		if (testCollision()){
			this.loc.y++;
			if (touchDownGrace){
				this.touchDown();
			}
			else{
				//give the player another move period 
				touchDownGrace = true;
			}
		}
		else{
			touchDownGrace = false;
		}
	}

	private void touchDown() {
		for(int i = 0; i < 4 + mutation; i++){
			//reached top...
			if (blocks[i].getLocation().y >= Constants.gridHeight){
				this.ownerGrid.endGame();
				return;
			}
			else{
				blocks[i].fixBlock(this.ownerGrid);
			}
		}
		this.ownerGrid.touchDown();

	}
	
	

	public void slamDown() {
		while(!testCollision()){
			this.loc.y--;
		}
		this.loc.y++;
		this.touchDown();		
	}
	
	
}
