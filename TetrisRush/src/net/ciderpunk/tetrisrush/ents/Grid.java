package net.ciderpunk.tetrisrush.ents;

import java.util.Iterator;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;

import net.ciderpunk.tetrisrush.Constants;
import net.ciderpunk.tetrisrush.data.Coord;
import net.ciderpunk.tetrisrush.gui.GridManager;

public class Grid {

	protected Array<Row> blockMap;
	GridManager owner;
	
	public Grid(GridManager gridManager) {
		this.owner = gridManager;
		init();
	}


	public void init(){
		blockMap = new Array<Row>(true, Constants.gridHeight);
		//blockMap = new Block[Constants.gridHeight][Constants.gridWidth];
		for (int y = 0; y < Constants.gridHeight; y++){
			blockMap.add(new Row(this));	
		}
	}
	
	public void draw(SpriteBatch batch){
		for (int y = 0; y < Constants.gridHeight; y++){
			blockMap.get(y).draw(batch, y);
		}
	}
	
	public Block get(int x, int y) {
		return blockMap.get(y).get(x);
	}

	public Block get(Coord loc) {
		return get(loc.x,loc.y);
	}

	public void set(int x, int y, Block block) {
		blockMap.get(y).set(x, block);
	}

	public void set(Coord loc, Block block) {
		set(loc.x,loc.y, block);
	}

	public int testRows(){
		int rowsCleared = 0;
		Iterator<Row> i = blockMap.iterator();
		while(i.hasNext()){
			Row row = i.next();
			if (row.testFull()){
				rowsCleared++;
			}
		}
		return rowsCleared;
	}
	
	public void think(float dT) {
		Iterator<Row> i = blockMap.iterator();
		while(i.hasNext()){
			Row row = i.next();
			row.think(dT);
		}
	}


	public void recycleRow(Row row) {
		//remove row
		blockMap.removeValue(row, true);
		//add to top
		blockMap.add(row);
	}


	public void reset() {
		Iterator<Row> i = blockMap.iterator();
		while(i.hasNext()){
			Row row = i.next();
			row.reset();
			
		}
	}
}
