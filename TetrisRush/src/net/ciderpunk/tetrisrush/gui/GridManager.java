package net.ciderpunk.tetrisrush.gui;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import net.ciderpunk.gamebase.gui.GuiElement;
import net.ciderpunk.gamebase.input.IKeyListener;
import net.ciderpunk.gamebase.resources.ResourceManager;
import net.ciderpunk.tetrisrush.Constants;
import net.ciderpunk.tetrisrush.data.Coord;
import net.ciderpunk.tetrisrush.data.Shape;
import net.ciderpunk.tetrisrush.ents.Block;
import net.ciderpunk.tetrisrush.ents.Grid;
import net.ciderpunk.tetrisrush.ents.Tetramino;

/**
 * @author Matthew
 *
 */
public class GridManager extends GuiElement implements IKeyListener{

	protected GridFrame gridFrame;
	protected ShapeFrame shapeFrame;
	protected ScoreFrame scoreFrame;
	protected TimeFrame timeFrame;
	protected OrthographicCamera gridCamera;
	protected OrthographicCamera previewCamera;
	
	
	protected Tetramino[] tetraPool;
	int activeTetraIndex;
	
	int tetraminoCount;

	int movement;
	float moveCycle;
	float gridScale;
	boolean fastDrop = false;
	float fallTimer = 0f;
	

	Grid grid;
	int level = 0;
	int score = 0;
	int rowsCleared = 0;
	int mutation = 0;
	float mutateTimer;
	
	int gridWidth;

	public float getFallRate() {
		return 0.1f+ ((10 - level) * 0.05f);
	}

	public void resetGame(){
		level = 0;
		score = 0;
		rowsCleared = 0;
		mutation = 0;
		this.fastDrop = false;
		this.resetMutationTimer(false);
		tetraPool = new Tetramino[2];
		this.grid.reset();
		for (int i = 0; i < tetraPool.length; i++ ){
			tetraPool[i] = new Tetramino(this);
			tetraPool[i].init(tetraminoCount++,this.mutation, Shape.shapes[MathUtils.random(Shape.shapes.length-1)], Constants.gridWidth / 2, Constants.gridHeight);
		}
		this.activeTetraIndex = 0;
	}
	
	public GridManager() {
		tetraminoCount = 0;
		grid = new Grid(this);
		this.addChild(this.gridFrame = new GridFrame());	
		this.addChild(this.shapeFrame = new ShapeFrame());
		this.addChild(this.scoreFrame = new ScoreFrame());
		this.addChild(this.timeFrame = new TimeFrame());
	}

	@Override
	public void updatePosition() {
		this.height = parent.getHeight();
		this.width =  parent.getWidth();
		int gridHeight = this.height -16;
		float blockSize = (float)gridHeight / (float)Constants.gridHeight;
		int gridWidth = MathUtils.ceil( blockSize * (float)Constants.gridWidth) + 32; 
		this.gridFrame.resize(gridWidth, this.height);		
		this.gridScale = Constants.TileSize /  blockSize; 

		int vertical = MathUtils.ceil( (blockSize * 7) + 32);
		this.shapeFrame.resize(this.width - gridWidth, vertical);
		this.shapeFrame.setxOffs(gridWidth);
		this.shapeFrame.setyOffs(0);
		
		this.scoreFrame.resize(this.width - gridWidth, 120);
		this.scoreFrame.setxOffs(gridWidth);
		this.scoreFrame.setyOffs(vertical);
		

		this.timeFrame.resize(this.width - gridWidth, 90);
		this.timeFrame.setxOffs(gridWidth);
		this.timeFrame.setyOffs(vertical + 120);
		
		super.updatePosition();

		this.gridCamera = new OrthographicCamera(this.getWidth() * gridScale, this.getHeight() * gridScale );
		this.gridCamera.translate((gridCamera.viewportWidth / 2) - (16f * gridScale), (gridCamera.viewportHeight /2) - (16f * gridScale));
		
		this.previewCamera = new OrthographicCamera(this.getWidth() * gridScale, this.getHeight() * gridScale );
		this.previewCamera.translate((previewCamera.viewportWidth / 2) - ((this.shapeFrame.getxPos() + (this.shapeFrame.getWidth() / 2)) * this.gridScale),
			(previewCamera.viewportHeight /2)- ((this.shapeFrame.getyPos() + (this.shapeFrame.getHeight() / 2)) * this.gridScale));
	}

	public Tetramino getActive(){
		return tetraPool[this.activeTetraIndex];
	}
	
	public Tetramino getNext(){
		int next = activeTetraIndex + 1 >= tetraPool.length ? 0 : activeTetraIndex + 1;
		return tetraPool[next];
	}
	
	public void switchActive(){
		if (++this.activeTetraIndex >= this.tetraPool.length){
			this.activeTetraIndex = 0;
		}
	}
	
	protected void drawGrid(SpriteBatch batch){
		gridCamera.update();
		batch.setProjectionMatrix(gridCamera.combined);
		batch.begin();
		getActive().draw(batch);
		grid.draw(batch);
		batch.end();
		
		previewCamera.update();
		batch.setProjectionMatrix(previewCamera.combined);
		batch.begin();
		this.getNext().draw(batch, true);
		batch.end();
	}
	
	@Override
	public void attach(GuiElement newParent) {
		super.attach(newParent);
		((TetrisRushGame)newParent).getInputListener().registerKeyListener(this);
	}



	protected static Sound clearSfx;
	protected static Sound superClearSfx;
	protected static Sound gameoverSfx;
	protected static Sound mutateSfx;
	protected static Sound touchdownSfx;
	protected static Sound rotateSfx;
	
	@Override
	public void preLoad(ResourceManager resMan) {
		super.preLoad(resMan);
		resMan.getAssetMan().load("sound/clear.wav", Sound.class);
		resMan.getAssetMan().load("sound/superclear.wav", Sound.class);
		resMan.getAssetMan().load("sound/gameover.wav", Sound.class);
		resMan.getAssetMan().load("sound/mutate.wav", Sound.class);
		resMan.getAssetMan().load("sound/touchdown.wav", Sound.class);
		resMan.getAssetMan().load("sound/rotate.wav", Sound.class);
	}

	@Override
	public void postLoad(ResourceManager resMan) {
		// TODO Auto-generated method stub
		super.postLoad(resMan);
		clearSfx = resMan.getAssetMan().get("sound/clear.wav", Sound.class);
		superClearSfx = resMan.getAssetMan().get("sound/superclear.wav", Sound.class);
		gameoverSfx = resMan.getAssetMan().get("sound/gameover.wav", Sound.class);
		mutateSfx = resMan.getAssetMan().get("sound/mutate.wav", Sound.class);
		touchdownSfx = resMan.getAssetMan().get("sound/touchdown.wav", Sound.class);
		rotateSfx = resMan.getAssetMan().get("sound/rotate.wav", Sound.class);
	}

	public boolean isOccupied(Coord loc){
		if (loc.x >= 0 && loc.x < Constants.gridWidth && loc.y >= 0 ){
			return (loc.y < Constants.gridHeight && grid.get(loc) != null);
		}
		return true;
	}
	
	public void think(float dT){	
		//lateral movement
		if (movement != 0 && (moveCycle -= dT) < 0f){
			getActive().move(movement);
			moveCycle += 0.02f;
		}

		float currentRate = (this.fastDrop ? Constants.FastFallRate : this.getFallRate());
		if ((fallTimer += dT) > currentRate){
			getActive().fallDown();
			fallTimer -= currentRate;
		}
		grid.think(dT);
		
		
		//update timer
		this.mutateTimer -= dT;
		if (this.mutateTimer < 0 && this.mutation < Constants.MaxMutation){
			this.mutation++;
			resetMutationTimer(true);
			mutateSfx.play();
		}


	}

	public void resetMutationTimer(boolean grace){
		this.mutateTimer = Constants.MutationTimerStart + (grace? Constants.MutationTimerGrace : 0f);
	}
	
	
	@Override
	public boolean keyDown(int keyCode) {
		switch(keyCode){
			case 21:
			case 29:
				movement = -1;
				getActive().move(movement);
				moveCycle = 0.3f;
				return true;
			case 22:
			case 32:
				movement = 1;
				getActive().move(movement);
				moveCycle = 0.3f;
				return true;
			case 20:
			case 47:
				this.fastDrop  = true;
				return true;
			case 19:
			case 52:
			case 33:
			case 44:
			case 51:
				if (getActive().rotate(true)){
					rotateSfx.play(0.5f);
				}
				return true;
			case 54:
			case 45:
			case 43:
				if (getActive().rotate(false)){
					rotateSfx.play(0.5f);
				}
				return true;
			case 48:
			//	activePiece = null;
				break;
			case 62:
				getActive().slamDown();
				return true;
		}
		return false;
	}
	
	@Override
	public boolean keyUp(int keyCode) {
		//System.out.printf("keyUp: %d\n", keyCode );
		switch(keyCode){
			case 21:
			case 29:
			case 22:
			case 32:
				movement = 0;
				return true;
			case 20:
			case 47:
				this.fastDrop = false;
				return true;
		}
		return false;
	}

	public void addBlock(Coord loc, Block block) {
		grid.set(loc, block);
	}

	public void endGame(){
		gameoverSfx.play();
		this.getRoot().endGame();
	}
	
	public void testRows(){
		int count = grid.testRows();
		if (count > 0){
			this.rowsCleared += count;
			this.level = MathUtils.floor(rowsCleared / 10f);
			this.level = level > 10 ? 10 : level;
			this.score += count * count * (100  + (20 * level));
			resetMutationTimer(false);
			if (count > 3){
				clearSfx.play();
				mutation = 0;
			}
			else{
				superClearSfx.play();
				mutation -= count;
				mutation = mutation < 0 ? 0 : mutation;
			}	
		}
	}

	@Override
	public TetrisRushGame getRoot() {
		// TODO Auto-generated method stub
		return (TetrisRushGame)super.getRoot();
	}

	public void touchDown() {
		this.fastDrop = false;
		touchdownSfx.play(1f);
		
		//trigger a row test
		this.testRows();
		this.getActive().init(tetraminoCount++,this.mutation, Shape.shapes[MathUtils.random(Shape.shapes.length-1)], Constants.gridWidth / 2, Constants.gridHeight);
		this.switchActive();
	}

	public int getLevel() {
		return level;
	}

	public int getScore() {
		return score;
	}

	public int getRowsCleared() {
		return rowsCleared;
	}

	public int getMutation() {
		return mutation;
	}
	
	public float getMutationTime(){
		return this.mutateTimer > Constants.MutationTimerStart || this.mutateTimer < 0 ? 0f : this.mutateTimer;
	}
	
}
