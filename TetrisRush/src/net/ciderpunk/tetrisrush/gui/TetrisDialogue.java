package net.ciderpunk.tetrisrush.gui;

import net.ciderpunk.gamebase.gui.Dialogue;
import net.ciderpunk.gamebase.gui.Theme;
import net.ciderpunk.gamebase.input.IKeyListener;

public class TetrisDialogue extends Dialogue implements IKeyListener {

	public TetrisDialogue(Theme theme, String message, String buttonText) {
		super(400, 200, HorizontalPosition.Mid , VerticalPosition.Mid, 0,0, theme, message, buttonText);

	}

	@Override
	public boolean keyDown(int keyCode) {
		switch(keyCode){
			case 66:
			case 62:
			this.doEvent(this);
				return true;
		}
		return false;
	}

	@Override
	public boolean keyUp(int keyCode) {
		// TODO Auto-generated method stub
		return false;
	}

}
