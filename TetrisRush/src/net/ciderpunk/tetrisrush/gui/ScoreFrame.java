package net.ciderpunk.tetrisrush.gui;

import net.ciderpunk.gamebase.gui.GuiElement;
import net.ciderpunk.gamebase.resources.ResourceManager;
import net.ciderpunk.gamebase.utils.NumberFormatter;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;

public class ScoreFrame extends DisplayFrame {
	String score = "", rows = "", level = "", mutation = "";
	int lastScore = -1, lastRows = -1, lastLevel = -100, lastMutation = -1;
	
	
	@Override
	public void update(float dT) {

		if (this.getParent().getRowsCleared() != lastScore){
			lastScore = this.getParent().getScore();
			this.score = "Score: " + NumberFormatter.formatNumber(lastScore);
		}
		if (this.getParent().getRowsCleared() != lastRows){
			lastRows = this.getParent().getRowsCleared();
			this.rows = "Rows cleared: " + lastRows;
		}

		if (this.getParent().getLevel() != lastLevel){
			lastLevel = this.getParent().getLevel();
			this.level = "Level: " + (lastLevel + 1);
		}
		
		
		if (this.getParent().getMutation() != lastMutation){
			lastMutation = this.getParent().getMutation();
			this.mutation = lastMutation > 0 ?  "Mutation: " + lastMutation : "";
		}
	}

	@Override
	protected void doDraw(SpriteBatch batch, int x, int y) {
		super.doDraw(batch, x, y);
		int space = MathUtils.ceil(theme.getFont().getLineHeight());
		theme.getFont().draw(batch, score, x + 20, y + this.getHeight() - space);
		theme.getFont().draw(batch, rows, x + 20, y + this.getHeight() - space * 2);
		theme.getFont().draw(batch, level, x + 20, y + this.getHeight() - space * 3);
		theme.getFont().draw(batch, mutation, x + 20, y + this.getHeight() - space * 4);
	}
	
}
