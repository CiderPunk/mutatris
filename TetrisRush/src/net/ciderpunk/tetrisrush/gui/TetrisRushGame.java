package net.ciderpunk.tetrisrush.gui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import net.ciderpunk.gamebase.events.IListener;
import net.ciderpunk.gamebase.gui.GameScreen;
import net.ciderpunk.gamebase.gui.GuiElement;
import net.ciderpunk.gamebase.gui.Theme;
import net.ciderpunk.gamebase.resources.ResourceManager;
import net.ciderpunk.gamebase.utils.NumberFormatter;
import net.ciderpunk.tetrisrush.ents.Block;

public class TetrisRushGame extends GameScreen  {

	boolean active;
	GridManager grid;
	private int widthActual;
	private int heightActual;
	private float inputScaleX;
	private float inputScaleY;
	TetrisDialogue startDialogue;
	
	Theme theme;
	
	public TetrisRushGame(int w, int h) {
		super(w, h);
		active = true;				
	}

	@Override
	protected void think(float dT) {
		if (active){
			grid.think(dT);
		}
	}

	@Override
	protected void doDraw(SpriteBatch batch, int x, int y) {
		Gdx.gl.glClearColor(0.2f, 0.2f, 0.2f, 0);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		grid.drawGrid(batch);
		camera.update();
		batch.setProjectionMatrix(camera.combined);
		batch.begin();
		super.doDraw(batch, x, y);
		batch.end();
	}

	@Override
	public void startGame() {
		resetGame();
	}
	
	
	public void resetGame(){
		this.getInputListener().registerKeyListener(grid);
		grid.resetGame();
		this.active= true;
	}

	public void pauseGame() {
		active = false;
		this.getInputListener().unRegisterKeyListener(grid);
	}

	public void resumeGame() {
		active = true;
		this.getInputListener().registerKeyListener(grid);
	}

	@Override
	public void preLoad(ResourceManager resMan) {
		this.theme = Theme.LoadTheme("theme.json");
		resMan.addResourceUser(this.theme);
		grid = new GridManager();
		this.addChild(grid);
		resMan.addResourceUser(new Block());
		super.preLoad(resMan);
	}

	@Override
	public void postLoad(ResourceManager resMan) {
		// TODO Auto-generated method stub
		super.postLoad(resMan);
	}
	
	@Override
	public void resize(int width, int height) {		
		super.resize(width, height);
	}

	public Theme getTheme() {
		return theme;
	}

	public void endGame() {
		
		String message = "Game Over\n\nYour final score was " + NumberFormatter.formatNumber(grid.getScore());
		TetrisDialogue dialogue = new TetrisDialogue( this.getTheme(), message,"Restart" );
		this.getInputListener().registerKeyListener(dialogue);
		dialogue.addListener(new IListener(){
			public void doEvent(Object source){
				TetrisRushGame game = (TetrisRushGame) self;
				game.removeChild((GuiElement)source);
				game.getInputListener().unRegisterKeyListener((TetrisDialogue)source);
	  		game.resetGame();
	  	}
		});
		this.addChild(dialogue);
		this.getInputListener().unRegisterKeyListener(grid);
		active = false;
	}

	@Override
	public float translateTouchY(float yTouch) {
		return yTouch;		
	}

	
	
}

