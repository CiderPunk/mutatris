package net.ciderpunk.tetrisrush.gui;

import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

import net.ciderpunk.gamebase.gui.GuiElement;
import net.ciderpunk.gamebase.gui.Theme;
import net.ciderpunk.gamebase.resources.ResourceManager;
import net.ciderpunk.tetrisrush.Constants;

public abstract class DisplayFrame extends GuiElement {
		
	Theme theme;
	
	protected String getThemePath(){
		return "scorepanel";
	}
	
	@Override
	public void preLoad(ResourceManager resMan) {
	
	}

	@Override
	protected void doDraw(SpriteBatch batch, int x, int y) {
		theme.getPatch().draw(batch, x, y, this.width, this.height);
	}
	
	
	@Override
	public void postLoad(ResourceManager resMan) {
		theme = ((TetrisRushGame)this.getRoot()).theme.getTheme(this.getThemePath());
		super.postLoad(resMan);
	}	
	@Override
	public GridManager getParent() {
		// TODO Auto-generated method stub
		return (GridManager) super.getParent();
	}


}
