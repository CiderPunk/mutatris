package net.ciderpunk.tetrisrush;

import net.ciderpunk.tetrisrush.gui.TetrisRushGame;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;


public class TetrisRush implements ApplicationListener {

	private TetrisRushGame game;
	@Override
	public void create() {		
		game = new TetrisRushGame(Gdx.graphics.getWidth(),Gdx.graphics.getHeight());	
	}
	
	@Override
	public void dispose() {
		game.dispose();
	}

	@Override
	public void render(){		
		game.update( Gdx.graphics.getDeltaTime() );
	}

	@Override
	public void resize(int width, int height) {
		game.resize(width, height);
	}

	@Override
	public void pause() {
		game.pauseGame();
	}

	@Override
	public void resume() {
		game.resumeGame();
	}
}
