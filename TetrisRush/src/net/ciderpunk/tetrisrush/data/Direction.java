package net.ciderpunk.tetrisrush.data;

public enum Direction {
	clockwise,
	counterClockwise,
}
