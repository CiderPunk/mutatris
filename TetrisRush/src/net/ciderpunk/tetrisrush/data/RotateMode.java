package net.ciderpunk.tetrisrush.data;

public enum RotateMode {
	fourWay,
	twoWay,
	oneWay,
}
