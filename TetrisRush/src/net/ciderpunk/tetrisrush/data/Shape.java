package net.ciderpunk.tetrisrush.data;

import com.badlogic.gdx.graphics.Color;

public class Shape {
	
	public static Shape[] shapes = {  
		new Shape(new Color(1f,0,0,1f), RotateMode.twoWay, new Coord[] { new Coord(0,0),  new Coord(-1,0),  new Coord(1,0), new Coord(2,0)}),  //I red
		new Shape(new Color(1f,0.5f,0,1f), RotateMode.fourWay, new Coord[] { new Coord(0,0),  new Coord(-1,0), new Coord(-1,-1), new Coord(1,0) }), //L orange
		new Shape(new Color(0,0,1f,1f), RotateMode.fourWay, new Coord[] { new Coord(0,0),  new Coord(-1,0), new Coord(1,-1), new Coord(1,0) }), //J blue
		new Shape(new Color(0,1f,1f,1f), RotateMode.oneWay, new Coord[] { new Coord(0,0),  new Coord(-1,0), new Coord(-1,-1), new Coord(0,-1) }), //O  cyan
		new Shape(new Color(0,1f,0,1f), RotateMode.twoWay, new Coord[] { new Coord(0,0),  new Coord(-1,0), new Coord(0,-1), new Coord(1,-1) }), //S green
		new Shape(new Color(1f,1f,0,1f), RotateMode.fourWay, new Coord[] { new Coord(0,0),  new Coord(-1,0), new Coord(0,-1), new Coord(1,0) }), //T pink
		new Shape(new Color(1f,0,1f,1f), RotateMode.twoWay, new Coord[] { new Coord(0,0),  new Coord(0,-1), new Coord(-1,-1), new Coord(1,0) }), //Z yellow
	};

	public Shape(Color col, RotateMode rot, Coord[] coords){ 
		this.colour = col;
		this.rot = rot;
		this.blockCoords = coords;
	}
	public RotateMode rot;
	public Color colour;
	public Coord[] blockCoords;
}
