package net.ciderpunk.tetrisrush.data;

public class Coord{
	
	
	public static Coord[] directions = new Coord[]{ 
		new Coord(0,1),
		new Coord(1,0),
		new Coord(-1,0),
		new Coord(0,-1),
		};
	
	public static Coord zero = new Coord();
	public static Coord temp = new Coord();
	
	public Coord(Coord copy){ this(copy.x,copy.y);}
	public Coord(){ this(0,0);}
	public Coord (int X, int Y){
		this.x = X;
		this.y= Y;
	}
 
	public int x,y;
	public Coord set(int newX, int newY){
		this.x = newX; this.y = newY;
		return this;
	}
	public Coord set(Coord target){
		if (target!= null){
			return this.set(target.x, target.y);
		}
		else {
			return this.reset();
		}
	}
	
	public Coord reset(){
		return this.set(0,0);
	}
	
	public void rotateCCW(){
		int temp = this.x;
		this.x = -1 * this.y;
		this.y = temp;
	}
	
	public void rotateCW(){
		int temp = this.x;
		this.x = y;
		this.y = -1 * temp;
	}
	
	public Coord add(Coord target){
		this.x += target.x;
		this.y += target.y;
		return this;
	}
	public boolean Equal(Coord  test){
		if (test == null){return false;}
		return (this.x == test.x && this.y == test.y);
	}
}
