package net.ciderpunk.tetrisrush.client;

import net.ciderpunk.tetrisrush.TetrisRush;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.backends.gwt.GwtApplication;
import com.badlogic.gdx.backends.gwt.GwtApplicationConfiguration;

public class GwtLauncher extends GwtApplication {
	@Override
	public GwtApplicationConfiguration getConfig () {
		GwtApplicationConfiguration cfg = new GwtApplicationConfiguration(480, 480);
		return cfg;
	}

	@Override
	public ApplicationListener getApplicationListener () {
		return new TetrisRush();
	}
}